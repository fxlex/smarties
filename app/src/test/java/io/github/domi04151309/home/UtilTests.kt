package io.github.domi04151309.home

import android.view.View
import android.view.ViewGroup
import com.smarties.utils.Util.constrain
import com.smarties.utils.Util.isPointInsideView
import com.smarties.utils.Util.map
import org.junit.Test

import org.junit.Assert.*

class UtilTests {
    @Test
    fun map_isCorrect() {
        assertEquals(50f, map(5f,0f,10f,0f,100f))
        assertEquals(5f, map(5f,0f,10f,0f,10f))
    }

    @Test
    fun constrain_isCorrect() {
        assertEquals(-50, constrain(-100,-50,50))
        assertEquals(10, constrain(10,-50,50))
        assertEquals(50, constrain(1500,-50,50))
    }
}
