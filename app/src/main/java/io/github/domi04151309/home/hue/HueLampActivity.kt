package io.github.domi04151309.home.hue

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.*
import androidx.core.graphics.drawable.DrawableCompat
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import io.github.domi04151309.home.Global.volleyError
import io.github.domi04151309.home.data.ScenesGridItem
import java.lang.Exception
import android.view.animation.DecelerateInterpolator
import android.animation.ObjectAnimator
import android.content.Intent
import android.view.*
import android.widget.AdapterView
import android.widget.SeekBar
import androidx.appcompat.app.AlertDialog
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.github.nisrulz.sensey.Sensey
import com.smarties.utils.Util
import io.github.domi04151309.home.*
import org.json.JSONObject


class HueLampActivity : AppCompatActivity() {

    var isRoom: Boolean = false
    var queue: RequestQueue? = null
    var lightDataRequest: JsonObjectRequest? = null
    var roomDataRequest: JsonObjectRequest? = null
    var scenesRequest: JsonObjectRequest? = null
    var hueAPI: HueAPI? = null
    lateinit var id: String
    var address: String = ""
    private var selectedScene: CharSequence = ""
    private var selectedSceneName: CharSequence = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        Theme.set(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hue_lamp)

        val internId = intent.getStringExtra("ID") ?: "0"
        if (internId.startsWith("room#")) {
            id = internId.substring(internId.lastIndexOf("#") + 1)
            isRoom = true
        } else {
            id = internId
            isRoom = false
        }
        val deviceId = intent.getStringExtra("Device") ?: ""
        val device = Devices(this).getDeviceById(deviceId)
        address = device.address
        hueAPI = HueAPI(this, deviceId)
        queue = Volley.newRequestQueue(this)

        title = device.name
        val lampIcon = findViewById<ImageView>(R.id.lampIcon)
        val nameTxt = findViewById<TextView>(R.id.nameTxt)
        val briBar = findViewById<SeekBar>(R.id.briBar)
        val ctBar = findViewById<SeekBar>(R.id.ctBar)
        val hueBar = findViewById<SeekBar>(R.id.hueBar)
        val satBar = findViewById<SeekBar>(R.id.satBar)
        val gridView = findViewById<View>(R.id.scenes) as GridView

        //Reset tint
        DrawableCompat.setTint(
                DrawableCompat.wrap(lampIcon.drawable),
                Color.parseColor("#FBC02D")
        )


        //Get scenes


        // Selected item is a whole room
        if (isRoom) {
            roomDataRequest = JsonObjectRequest(Request.Method.GET, address + "api/" + hueAPI!!.getUsername() + "/groups/" + id, null,
                    Response.Listener { response ->
                        nameTxt.text = response.getString("name")
                        val action = response.getJSONObject("action")
                        if (action.has("bri")) {
                            setProgress(briBar, action.getInt("bri"))
                        } else {
                            findViewById<TextView>(R.id.briTxt).visibility = View.GONE
                            briBar.visibility = View.GONE
                        }
                        if (action.has("ct")) {
                            setProgress(ctBar, action.getInt("ct") - 153)
                        } else {
                            findViewById<TextView>(R.id.ctTxt).visibility = View.GONE
                            ctBar.visibility = View.GONE
                        }
                        if (action.has("hue")) {
                            setProgress(hueBar, action.getInt("hue"))
                        } else {
                            findViewById<TextView>(R.id.hueTxt).visibility = View.GONE
                            hueBar.visibility = View.GONE
                        }
                        if (action.has("sat")) {
                            setProgress(satBar, action.getInt("sat"))
                        } else {
                            findViewById<TextView>(R.id.satTxt).visibility = View.GONE
                            satBar.visibility = View.GONE
                        }
                    },
                    Response.ErrorListener { error ->
                        finish()
                        Toast.makeText(this, volleyError(this, error), Toast.LENGTH_LONG).show()
                    }
            )

            scenesRequest = JsonObjectRequest(Request.Method.GET, address + "api/" + hueAPI!!.getUsername() + "/scenes/", null,
                    Response.Listener { response ->
                        try {
                            var gridItems: Array<ScenesGridItem> = arrayOf()
                            var currentObjectName: String
                            var currentObject: JSONObject
                            for (i in 0 until response.length()) {
                                currentObjectName = response.names()!!.getString(i)
                                currentObject = response.getJSONObject(currentObjectName)
                                if (currentObject.getString("group") == id) {
                                    val scene = ScenesGridItem(currentObject.getString("name"))
                                    scene.hidden = currentObjectName
                                    scene.icon = R.drawable.ic_hue_scene
                                    gridItems += scene
                                }
                            }
                            val scene = ScenesGridItem(resources.getString(R.string.hue_add_scene))
                            scene.hidden = "add"
                            scene.icon = R.drawable.ic_hue_scene_add
                            gridItems += scene
                            val adapter = HueScenesGridAdapter(this, gridItems)
                            gridView.adapter = adapter
                        } catch (e: Exception){
                            Log.e(Global.LOG_TAG, e.toString())
                        }
                    },
                    Response.ErrorListener { error ->
                        finish()
                        Toast.makeText(this, volleyError(this, error), Toast.LENGTH_LONG).show()
                    }
            )

            findViewById<Button>(R.id.onBtn).setOnClickListener {
                hueAPI!!.switchGroupByID(id, true)
            }

            findViewById<Button>(R.id.offBtn).setOnClickListener {
                hueAPI!!.switchGroupByID(id, false)
            }

            briBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {}
                override fun onStartTrackingTouch(seekBar: SeekBar) {}
                override fun onStopTrackingTouch(seekBar: SeekBar) {
                    hueAPI!!.changeBrightnessOfGroup(id, seekBar.progress)
                }
            })

            ctBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                    DrawableCompat.setTint(
                            DrawableCompat.wrap(lampIcon.drawable),
                            HueUtils.ctToRGB(seekBar.progress + 153)
                    )
                }
                override fun onStartTrackingTouch(seekBar: SeekBar) {}
                override fun onStopTrackingTouch(seekBar: SeekBar) {
                    hueAPI!!.changeColorTemperatureOfGroup(id, seekBar.progress + 153)
                }
            })

            hueBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                    DrawableCompat.setTint(
                            DrawableCompat.wrap(lampIcon.drawable),
                            HueUtils.hueSatToRGB(seekBar.progress, satBar.progress)
                    )
                }
                override fun onStartTrackingTouch(seekBar: SeekBar) {}
                override fun onStopTrackingTouch(seekBar: SeekBar) {
                    hueAPI!!.changeHueOfGroup(id, seekBar.progress)
                }
            })

            satBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                    DrawableCompat.setTint(
                            DrawableCompat.wrap(lampIcon.drawable),
                            HueUtils.hueSatToRGB(hueBar.progress, seekBar.progress)
                    )
                }
                override fun onStartTrackingTouch(seekBar: SeekBar) {}
                override fun onStopTrackingTouch(seekBar: SeekBar) {
                    hueAPI!!.changeSaturationOfGroup(id, seekBar.progress)
                }
            })

            gridView.onItemClickListener = AdapterView.OnItemClickListener { _, view, _, _ ->
                val hiddenText = view.findViewById<TextView>(R.id.hidden).text.toString()
                if (hiddenText == "add") {
                    startActivity(Intent(this, HueSceneActivity::class.java).putExtra("deviceId", deviceId).putExtra("room", id))
                } else {
                    hueAPI!!.activateSceneOfGroup(id, hiddenText)
                    Handler().postDelayed({
                        queue!!.add(roomDataRequest)
                    }, 200)
                }
            }

            registerForContextMenu(gridView)
        }

        // Selected item is a single light
        else {
            lightDataRequest = JsonObjectRequest(Request.Method.GET,  address + "api/" + hueAPI!!.getUsername() + "/lights/" + id, null,
                Response.Listener { response ->
                    nameTxt.text = response.getString("name")
                    val state = response.getJSONObject("state")
                    if (state.has("bri")) {
                        setProgress(briBar, state.getInt("bri"))
                    } else {
                        findViewById<TextView>(R.id.briTxt).visibility = View.GONE
                        briBar.visibility = View.GONE
                    }
                    if (state.has("ct")) {
                        setProgress(ctBar, state.getInt("ct") - 153)
                    } else {
                        findViewById<TextView>(R.id.ctTxt).visibility = View.GONE
                        ctBar.visibility = View.GONE
                    }
                    if (state.has("hue")) {
                        setProgress(hueBar, state.getInt("hue"))
                    } else {
                        findViewById<TextView>(R.id.hueTxt).visibility = View.GONE
                        hueBar.visibility = View.GONE
                    }
                    if (state.has("sat")) {
                        setProgress(satBar, state.getInt("sat"))
                    } else {
                        findViewById<TextView>(R.id.satTxt).visibility = View.GONE
                        satBar.visibility = View.GONE
                    }
                },
                Response.ErrorListener { error ->
                    finish()
                    Toast.makeText(this, volleyError(this, error), Toast.LENGTH_LONG).show()
                }
            )

            gridView.visibility = View.GONE



            briBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                    if (fromUser) hueAPI!!.changeBrightness(id, seekBar.progress)
                }
                override fun onStartTrackingTouch(seekBar: SeekBar) {}
                override fun onStopTrackingTouch(seekBar: SeekBar) {}
            })

            ctBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                    if (fromUser) hueAPI!!.changeColorTemperature(id, seekBar.progress + 153)
                    DrawableCompat.setTint(
                            DrawableCompat.wrap(lampIcon.drawable),
                            HueUtils.ctToRGB(seekBar.progress + 153)
                    )
                }
                override fun onStartTrackingTouch(seekBar: SeekBar) {}
                override fun onStopTrackingTouch(seekBar: SeekBar) {}
            })

            hueBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                    if (fromUser) hueAPI!!.changeHue(id, seekBar.progress)
                    DrawableCompat.setTint(
                            DrawableCompat.wrap(lampIcon.drawable),
                            HueUtils.hueSatToRGB(seekBar.progress, satBar.progress)
                    )
                }
                override fun onStartTrackingTouch(seekBar: SeekBar) {}
                override fun onStopTrackingTouch(seekBar: SeekBar) {}
            })

            satBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                    if (fromUser) hueAPI!!.changeSaturation(id, seekBar.progress)
                    DrawableCompat.setTint(
                            DrawableCompat.wrap(lampIcon.drawable),
                            HueUtils.hueSatToRGB(hueBar.progress, seekBar.progress)
                    )
                }
                override fun onStartTrackingTouch(seekBar: SeekBar) {}
                override fun onStopTrackingTouch(seekBar: SeekBar) {}
            })
        }
    }

    //Smooth seekBars
    fun setProgress(seekBar: SeekBar, value: Int) {
        val animation = ObjectAnimator.ofInt(seekBar, "progress", value)
        animation.duration = 300
        animation.interpolator = DecelerateInterpolator()
        animation.start()
    }

    override fun dispatchTouchEvent(event: MotionEvent):Boolean {
    // Setup onTouchEvent for detecting type of touch gesture
    val touchArea = findViewById<TextView>(R.id.hitArea)
    if(Util.isPointInsideView(event.x,event.y,touchArea)) {
        Sensey.getInstance().setupDispatchTouchEvent(event);
        return true
    }
    return super.dispatchTouchEvent(event);
    }


    override fun dispatchKeyEvent(event:KeyEvent):Boolean {
    val action = event.action
    val keyCode = event.keyCode
    return when (keyCode) {
        KeyEvent.KEYCODE_VOLUME_UP,KeyEvent.KEYCODE_VOLUME_DOWN -> {
            if (action == KeyEvent.ACTION_DOWN) {
                val intent = Intent("volumeKey").apply { putExtra("keyCode", keyCode) }
                LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
            }
            true
        }
        else ->
            super.dispatchKeyEvent(event);
    }
}

    override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?) {
        super.onCreateContextMenu(menu, v, menuInfo)
        val info = menuInfo as AdapterView.AdapterContextMenuInfo
        val view = v as GridView
        selectedScene = view.getChildAt(info.position).findViewById<TextView>(R.id.hidden).text
        selectedSceneName = view.getChildAt(info.position).findViewById<TextView>(R.id.name).text
        if (selectedScene != "add") {
            menuInflater.inflate(R.menu.activity_hue_lamp_context, menu)
        }
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        if (item.title == resources.getString(R.string.str_rename)) {
            val nullParent: ViewGroup? = null
            val view = layoutInflater.inflate(R.layout.dialog_input, nullParent, false)
            val input = view.findViewById<EditText>(R.id.input)
            input.setText(selectedSceneName)
            AlertDialog.Builder(this)
                    .setTitle(R.string.str_rename)
                    .setMessage(R.string.hue_rename_scene)
                    .setView(view)
                    .setPositiveButton(android.R.string.ok) { _, _ ->
                        val requestObject = "{\"name\":\"" + input.text.toString() + "\"}"
                        val renameSceneRequest = CustomJsonArrayRequest(Request.Method.PUT, address + "api/" + hueAPI!!.getUsername() + "/scenes/$selectedScene", JSONObject(requestObject),
                                Response.Listener { queue!!.add(scenesRequest) },
                                Response.ErrorListener { e -> Log.e(Global.LOG_TAG, e.toString())}
                        )
                        queue!!.add(renameSceneRequest)
                    }
                    .setNegativeButton(android.R.string.cancel) { _, _ -> }
                    .show()
        } else if (item.title == resources.getString(R.string.str_delete)) {
            AlertDialog.Builder(this)
                    .setTitle(R.string.str_delete)
                    .setMessage(R.string.hue_delete_scene)
                    .setPositiveButton(android.R.string.ok) { _, _ ->
                        val deleteSceneRequest = CustomJsonArrayRequest(Request.Method.DELETE, address + "api/" + hueAPI!!.getUsername() + "/scenes/" + selectedScene, null,
                                Response.Listener { queue!!.add(scenesRequest) },
                                Response.ErrorListener { e -> Log.e(Global.LOG_TAG, e.toString())}
                        )
                        queue!!.add(deleteSceneRequest)
                    }
                    .setNegativeButton(android.R.string.cancel) { _, _ -> }
                    .show()
        }
        return super.onContextItemSelected(item)

    }

    public override fun onResume() {
        super.onResume()
        if(isRoom) {
            queue!!.add(roomDataRequest)
            queue!!.add(scenesRequest)
        } else queue!!.add(lightDataRequest)


    }
}
