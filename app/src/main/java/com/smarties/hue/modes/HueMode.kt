package com.smarties.hue.modes

import android.os.Handler
import com.smarties.hue.fragments.SensorFragment

class DiscoMode(fragment: SensorFragment) {
    private val handler = Handler()
    private val delay = 1000L
    private var active = false
    private val runnable = object : Runnable {

        override fun run() {
            fragment.setRandomColors()
            handler.postDelayed(this, delay)
        }
    }

    fun start() {
        active = true
        handler.postDelayed(runnable, 0)
    }

    fun stop() {
        active = false
        handler.removeCallbacks(runnable)
    }

    fun isActive() = active

}