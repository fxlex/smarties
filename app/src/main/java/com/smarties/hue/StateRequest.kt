package com.smarties.hue

import android.content.Context
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.VolleyError
import com.smarties.data.gestures.state.HueState
import com.smarties.data.gestures.state.RequestInfo


class StateRequest(private val requestInfo: RequestInfo, private val queue: RequestQueue, val context: Context, val  listener: StateListener) {
    private var lightDataRequest: JsonObjectRequest? = null
    private var roomDataRequest: JsonObjectRequest? = null
    private val hueState = HueState(false,-1,-1,-1, -1)

    interface StateListener {
        fun onState(state: HueState)
        fun onError(error: VolleyError)
    }

    fun action() {
        if (requestInfo.isRoom) {
            roomDataRequest = JsonObjectRequest(Request.Method.GET, requestInfo.address + "api/" + requestInfo.userName + "/groups/" + requestInfo.id, null,
                    Response.Listener { response ->
                        val action = response.getJSONObject("action")
                        if(action.has("on")) {
                            hueState.on = action.getBoolean("on")
                        }
                        if(action.has("hue")) {
                            hueState.hue = action.getInt("hue")
                        }
                        if(action.has("sat")) {
                            hueState.saturation = action.getInt("sat")
                        }
                        if(action.has("bri")) {
                            hueState.brightness = action.getInt("bri")
                        }
                        if(action.has("ct")) {
                            hueState.colorTemperature = action.getInt("ct" ) - 153
                        }
                        listener.onState(hueState)
                    },
                    Response.ErrorListener { error ->  listener.onError(error) }
            )

        }

        // Selected item is a single light
        else {
            lightDataRequest = JsonObjectRequest(Request.Method.GET,  requestInfo.address + "api/" + requestInfo.userName + "/lights/" + requestInfo.id, null,
                    Response.Listener { response ->
                        val state = response.getJSONObject("state")
                        if(state.has("on")) {
                            hueState.on = state.getBoolean("on")
                        }
                        if(state.has("hue")) {
                            hueState.hue = state.getInt("hue")
                        }
                        if(state.has("sat")) {
                            hueState.hue = state.getInt("sat")
                        }
                        if(state.has("bri")) {
                            hueState.brightness = state.getInt("bri")
                        }
                        if(state.has("ct")) {
                            hueState.colorTemperature = state.getInt("ct")  - 153
                        }
                        listener.onState(hueState)
                    },
                    Response.ErrorListener { error -> listener.onError(error) }
            )
        }
        if(requestInfo.isRoom) {
            queue.add(roomDataRequest)
        } else queue.add(lightDataRequest)
    }
}