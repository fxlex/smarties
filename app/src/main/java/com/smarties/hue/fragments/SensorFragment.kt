package com.smarties.hue.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.github.nisrulz.sensey.TiltDirectionDetector
import com.github.nisrulz.sensey.TouchTypeDetector
import com.smarties.utils.Util.map
import com.smarties.data.gestures.GestureListener
import com.smarties.data.gestures.GestureManager
import io.github.domi04151309.home.Global
import io.github.domi04151309.home.hue.HueAPI
import io.github.domi04151309.home.hue.HueLampActivity

import java.text.DecimalFormat
import com.android.volley.Response
import com.android.volley.VolleyError
import com.smarties.data.gestures.TweakableValues.BRIGTHNESS_INC
import com.smarties.data.gestures.TweakableValues.ROTATION_EVENT_SKIPS
import com.smarties.data.gestures.TweakableValues.SATURATION_INC
import com.smarties.data.gestures.TweakableValues.TEMPERATURE_INC
import com.smarties.data.gestures.TweakableValues.VOLUME_TRESHOLD
import com.smarties.data.gestures.state.ColorBounds
import com.smarties.data.gestures.state.HueState
import com.smarties.data.gestures.state.RequestInfo
import com.smarties.hue.StateRequest
import com.smarties.hue.modes.DiscoMode
import com.smarties.utils.PHUtilities
import com.smarties.utils.Util.constrain
import io.github.domi04151309.home.R

class SensorFragment : Fragment(), GestureListener {

    lateinit var gestureManager: GestureManager
    lateinit var requestInfo: RequestInfo

    // time of the last proximity event in nano seconds
    var lastProximityEvent: Long = 0

    var rotationEventSkip: Int = 0




    private val discoMode = DiscoMode(this)

    lateinit var savedActivity: HueLampActivity

    override fun onAttach(context: Context) {
        super.onAttach(context);
        if (context is HueLampActivity) {
            this.savedActivity = context
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return  inflater.inflate(R.layout.fragment_sensor, container, false)
    }

    override fun onStart() {
        super.onStart()
        gestureManager = GestureManager(this.activity!!, this)
        gestureManager.activate()

        val receiver = object: BroadcastReceiver() {

            override fun onReceive(context: Context?, intent: Intent?) {
                val keyCode = intent?.getIntExtra("keyCode", 0)

                this@SensorFragment.toggleGestures(keyCode == KeyEvent.KEYCODE_VOLUME_UP)
            }
        }

        val filter = IntentFilter().apply { addAction("volumeKey") }
        LocalBroadcastManager.getInstance(context!!).registerReceiver(receiver, filter)

        requestInfo = RequestInfo(savedActivity.address, hueApi().getUsername(), lights(), savedActivity.isRoom)
    }

    /**
     * Return the helper class that communicates with the Philips Hue bridge
     */
    private fun hueApi(): HueAPI {
        return savedActivity.hueAPI!!
    }

    fun toggleGestures(on: Boolean) {
        if(on) {
            gestureManager.activate()
            Toast.makeText(savedActivity, "Gestures activated", Toast.LENGTH_SHORT).show()
        } else {
            gestureManager.deactivate()
            Toast.makeText(savedActivity, "Gestures deactivated", Toast.LENGTH_SHORT).show()
        }
    }

    /**
     * Returns the id of the currently selected light or group of lights
     */
    private fun lights(): String {
        return savedActivity.id
    }

    fun setRandomColors() {
        val h: Float = (Math.random() * 360).toFloat()
        val s: Float = Math.random().toFloat()
        val b: Float = Math.random().toFloat()
        val color = Color.HSVToColor(arrayOf(h, s, b).toFloatArray())
        changeXY(PHUtilities.calculateXY(color,""))
    }

    private fun  setRed() {
        changeXY(PHUtilities.calculateXYFromRGB(255,0,0,""))
    }

    private fun  setBlue() {
        changeXY(PHUtilities.calculateXYFromRGB(0,0,255,""))
    }

    private fun  setYellow() {
        changeXY(PHUtilities.calculateXYFromRGB(255,255,0,""))
    }

    private fun  setWhite() {
        changeXY(PHUtilities.calculateXYFromRGB(255,255,255,""))
    }

    private fun changeHue(color: Number, fromUser: Boolean = true) {
        if(savedActivity.isRoom) {
            hueApi().changeHueOfGroup(lights(), color.toInt())
        } else {
            hueApi().changeHue(lights(), color.toInt())
        }
    }

    private fun changeSaturation(color: Number) {
        if(savedActivity.isRoom) {
            hueApi().changeSaturationOfGroup(lights(), color.toInt())
        } else {
            hueApi().changeSaturation(lights(), color.toInt())
        }
    }

    private fun changeBrightness(color: Number) {
        if(savedActivity.isRoom) {
            hueApi().changeBrightnessOfGroup(lights(), color.toInt())
        } else {
            hueApi().changeBrightness(lights(), color.toInt())
        }
    }

    private fun changeTemperature(value: Number) {
        if(savedActivity.isRoom) {
            hueApi().changeColorTemperatureOfGroup(lights(), value.toInt())
        } else {
            hueApi().changeColorTemperature(lights(), value.toInt())
        }

    }

    private fun changeXY(xy: FloatArray ) {
        if(savedActivity.isRoom) {
            hueApi().changeXYOfGroup(lights(), xy[0], xy[1])
        } else {
            hueApi().changeXY(lights(), xy[0], xy[1])
        }
    }


    private fun switchLights(on: Boolean) {
        activity!!.runOnUiThread {
            hueApi().switchGroupByID(lights(), on, Response.Listener { savedActivity.onResume() })
        }
    }

    private fun logGestureEvent(message: String, toast: Boolean) {
        val text = "Gestures detected: $message"
        Log.d(Global.LOG_TAG, text)
        if(toast) {
            Toast.makeText(savedActivity,text,Toast.LENGTH_SHORT).show()
        }
    }

    override fun onPause() {
        super.onPause()
        gestureManager.deactivate()

    }

    override fun onDestroy() {
        super.onDestroy()
        gestureManager.destroy()
    }


    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String>,
                                            @NonNull grantResults: IntArray) {
        gestureManager.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }


    override fun onFaceDown() {
        logGestureEvent("Face Down", true)
        switchLights(false)
    }

    override fun onFaceUp() {
        logGestureEvent("Face Up", true)
        switchLights(true)
    }

    override fun onNear() {
        logGestureEvent("Near", false)
        this.lastProximityEvent = System.nanoTime()
    }

    override fun onFar() {
        logGestureEvent("Far", false)
        val currentTime = System.nanoTime()

        val elapsedTimeInSeconds = (currentTime - this.lastProximityEvent) / 1000000000
        if(elapsedTimeInSeconds in 2..10) {
            setRandomColors()
        }
    }

    override fun onLeftSideUp() {
        logGestureEvent("Left Side Up", true)
        setBlue()
    }


    override fun onRightSideUp() {
        logGestureEvent("Right Side Up", true)
        setRed()
    }

    override fun onTopSideUp() {
        logGestureEvent("Top Side Up", true)
        setWhite()
    }

    override fun onBottomSideUp() {
        logGestureEvent("Bottom Side Up", true)
        setYellow()
    }


    override fun onMovement() {
        logGestureEvent("Movement Detected", true)
        StateRequest(requestInfo, savedActivity.queue!!, savedActivity, object: StateRequest.StateListener {
            override fun onState(state: HueState) {
                switchLights(!state.on)
            }

            override fun onError(error: VolleyError) {
                Toast.makeText(savedActivity, error.message, Toast.LENGTH_SHORT).show()
            }

        }).action()
    }


    override fun onRotation(angleInAxisX: Float, angleInAxisY: Float, angleInAxisZ: Float) {

        if(rotationEventSkip > 0) {
            rotationEventSkip--
        return
        } else {
            rotationEventSkip = ROTATION_EVENT_SKIPS
        }

        logGestureEvent("Rotation in Axis Detected(deg):\nX="
                + angleInAxisX
                + ",\nY="
                + angleInAxisY
                + ",\nZ="
                + angleInAxisZ, false)
        val h = map(angleInAxisX,-180f,180f, 0f, 360f)
        val s = map(angleInAxisY,-180f,180f, 0f, 1f)
        val b = map(angleInAxisZ,-180f,180f, 0f, 1f)
        logGestureEvent("h:$h s:$s b:$b",false)
        val color = Color.HSVToColor(arrayOf(h, s, b).toFloatArray())
        changeXY(PHUtilities.calculateXY(color,""))
    }

    override fun onSoundDetected(level: Float) {
        logGestureEvent(DecimalFormat("##.##").format(level) + "dB", false)
        if(level >= VOLUME_TRESHOLD) {
            setRed()
        }
    }

    override fun onWave() {
        logGestureEvent("Wave Detected", true)
        if(discoMode.isActive()) {
            discoMode.stop()
        } else {
            discoMode.start()
        }
    }

    override fun onScale(scaleGestureDetector: ScaleGestureDetector, isScalingOut: Boolean) {

        logGestureEvent("Scaling" + ( if(isScalingOut) "Out" else "In"), true)

        StateRequest(requestInfo, savedActivity.queue!!, savedActivity, object: StateRequest.StateListener {
            override fun onState(state: HueState) {
                val oldTemperature = state.colorTemperature
                var temperature = oldTemperature
                if(isScalingOut) {
                    temperature += TEMPERATURE_INC
                } else {
                    temperature -= TEMPERATURE_INC
                }
                temperature = constrain(temperature, ColorBounds.CT.low, ColorBounds.CT.high)
                changeTemperature(temperature)
            }

            override fun onError(error: VolleyError) {
                Toast.makeText(savedActivity, error.message, Toast.LENGTH_SHORT).show()
            }

        }).action()
    }

    override fun onShakeDetected() {
        setRandomColors()
        logGestureEvent("Shake Detected", true)
    }


    override fun onDoubleTap() {
        changeHue(Math.random() * ColorBounds.HUE.high)
        logGestureEvent("Double Tap", true)
    }

    override fun onSwipe(swipeDirection: Int) {
        when (swipeDirection) {
            TouchTypeDetector.SWIPE_DIR_UP -> {
                logGestureEvent("Swiping Up", true)
                StateRequest(requestInfo, savedActivity.queue!!, savedActivity, object: StateRequest.StateListener {
                    override fun onState(state: HueState) {
                        val bri = constrain(state.brightness - BRIGTHNESS_INC, ColorBounds.BRI.low, ColorBounds.BRI.high)
                        changeBrightness(bri)
                    }

                    override fun onError(error: VolleyError) {
                        Toast.makeText(savedActivity, error.message, Toast.LENGTH_SHORT).show()
                    }
                }).action()
            }
            TouchTypeDetector.SWIPE_DIR_DOWN -> {
                logGestureEvent("Swiping Down", true)
                StateRequest(requestInfo, savedActivity.queue!!, savedActivity, object: StateRequest.StateListener {
                    override fun onState(state: HueState) {
                        val bri = constrain(state.brightness + BRIGTHNESS_INC, ColorBounds.BRI.low, ColorBounds.BRI.high)
                        changeBrightness(bri)
                    }

                    override fun onError(error: VolleyError) {
                        Toast.makeText(savedActivity, error.message, Toast.LENGTH_SHORT).show()
                    }
                }).action()
            }
            TouchTypeDetector.SWIPE_DIR_LEFT -> {
                logGestureEvent("Swiping Left", true)
                StateRequest(requestInfo, savedActivity.queue!!, savedActivity, object: StateRequest.StateListener {
                    override fun onState(state: HueState) {
                        val sat = constrain(state.saturation - SATURATION_INC, ColorBounds.SAT.low, ColorBounds.SAT.high)
                        changeSaturation(sat)
                    }

                    override fun onError(error: VolleyError) {
                        Toast.makeText(savedActivity, error.message, Toast.LENGTH_SHORT).show()
                    }
                }).action()
            }
            TouchTypeDetector.SWIPE_DIR_RIGHT -> {
                logGestureEvent("Swiping Right", true)
                StateRequest(requestInfo, savedActivity.queue!!, savedActivity, object: StateRequest.StateListener {
                    override fun onState(state: HueState) {
                        val sat = constrain(state.saturation + SATURATION_INC,  ColorBounds.SAT.low, ColorBounds.SAT.high)
                        changeSaturation(sat)
                    }

                    override fun onError(error: VolleyError) {
                        Toast.makeText(savedActivity, error.message, Toast.LENGTH_SHORT).show()
                    }
                }).action()
            }
            else -> {
            }
        }
    }


    override fun onThreeFingerSingleTap() {
        logGestureEvent("Three Finger Tap", false)
    }

    override fun onTwoFingerSingleTap() {
        logGestureEvent("Two Finger Tap", true)
        StateRequest(requestInfo, savedActivity.queue!!, savedActivity, object: StateRequest.StateListener {
            override fun onState(state: HueState) {
                switchLights(!state.on)
            }

            override fun onError(error: VolleyError) {
                Toast.makeText(savedActivity, error.message, Toast.LENGTH_SHORT).show()
            }

        }).action()
    }

    // The following gestures are currently not used:

    override fun onChop() {
        logGestureEvent("Chop", false)
    }

    override fun onDark() {
        logGestureEvent("Dark", false)
    }

    override fun onLight() {
        logGestureEvent("Not Dark", false)
    }

    private fun displayTiltDirection(direction: Int, axis: String) {
        val dir = if (direction == TiltDirectionDetector.DIRECTION_CLOCKWISE) "ClockWise" else  "AntiClockWise"
        logGestureEvent("Tilt in $axis Axis: $dir", false)
    }

    override fun onTiltInAxisX(direction: Int) {
        displayTiltDirection(direction, "X")
    }

    override fun onTiltInAxisY(direction: Int) {
        displayTiltDirection(direction, "Y")
    }

    override fun onTiltInAxisZ(direction: Int) {
        displayTiltDirection(direction, "Z")
    }

    override fun onSingleTap() {
        logGestureEvent("Single Tap", false)
    }

    override fun onLongPress() {
        logGestureEvent("Long press", false)
    }

    override fun onShakeStopped() {
        logGestureEvent("Shake Stopped!", false)
    }

    override fun onWristTwist() {
        logGestureEvent("Wrist Twist Detected!", false)
    }

    override fun onScaleStart(scaleGestureDetector: ScaleGestureDetector) {
        logGestureEvent("Scaling : Started", false)
    }

    override fun onScaleEnd(scaleGestureDetector: ScaleGestureDetector) {
        logGestureEvent("Scaling : Stopped", false)
    }

    override fun onStationary() {
        logGestureEvent("Device Stationary!", false)
    }

    override fun onScroll(scrollDirection: Int) {
        when (scrollDirection) {
            TouchTypeDetector.SCROLL_DIR_UP -> {
                logGestureEvent("Scrolling Up", true)
            }
            TouchTypeDetector.SCROLL_DIR_DOWN -> {
                logGestureEvent("Scrolling Down", true)
            }
            TouchTypeDetector.SCROLL_DIR_LEFT -> {
                logGestureEvent("Scrolling Left", true)
            }
            TouchTypeDetector.SCROLL_DIR_RIGHT -> {
                logGestureEvent("Scrolling Right", true)
            }
            else -> {
            }
        }
    }
}
