package com.smarties.data.gestures

import android.view.ScaleGestureDetector

/**
 * Adapter that implements all gestures
 */
interface GestureAdapter: GestureListener {
    override fun onShakeDetected() {}

    override fun onShakeStopped() {}

    override fun onFaceUp() {}

    override fun onFaceDown() {}

    override fun onLight() {}

    override fun onDark() {}

    override fun onBottomSideUp() {}

    override fun onLeftSideUp() {}

    override fun onRightSideUp() {}

    override fun onTopSideUp() {}

    override fun onFar() {}

    override fun onNear() {}

    override fun onWave() {}

    override fun onSoundDetected(p0: Float) {}

    override fun onStationary() {}

    override fun onMovement() {}

    override fun onChop() {}

    override fun onWristTwist() {}

    override fun onRotation(p0: Float, p1: Float, p2: Float) {}

    override fun onTiltInAxisX(p0: Int) {}

    override fun onTiltInAxisY(p0: Int) {}

    override fun onTiltInAxisZ(p0: Int) {}

    override fun onScaleEnd(p0: ScaleGestureDetector?) {}

    override fun onScale(detector: ScaleGestureDetector?, p1: Boolean) {}

    override fun onScaleStart(p0: ScaleGestureDetector?) {}

    override fun onDoubleTap() {}

    override fun onSwipe(p0: Int) {}

    override fun onSingleTap() {}

    override fun onScroll(p0: Int) {}

    override fun onLongPress() {}

    override fun onThreeFingerSingleTap() {}

    override fun onTwoFingerSingleTap() {}

}