package com.smarties.data.gestures

import android.Manifest
import android.app.Activity
import android.util.Log
import androidx.annotation.NonNull
import com.github.nisrulz.sensey.Sensey
import com.smarties.utils.RPResultListener
import com.smarties.utils.RuntimePermissionUtil


/**
 * Class that manages all the different gestures
 */
class GestureManager(private val activity: Activity, private val listener: GestureListener) {
    companion object {
        const val LOG_TAG = "GestureManager"
    }

    var hasRecordAudioPermission = false
    val recordAudioPermission = Manifest.permission.RECORD_AUDIO

    init {
        hasRecordAudioPermission = RuntimePermissionUtil.checkPermissionGranted(activity, recordAudioPermission)
        Sensey.getInstance().init(activity, Sensey.SAMPLING_PERIOD_NORMAL)
    }

    /**
     * Requests the android audio permission for the sound level gesture
     */
    fun requestAudioPermission() {
        RuntimePermissionUtil.requestPermission(activity, recordAudioPermission, 100)
    }

    /**
     * Checks if the audio permission was granted
     * @param requestCode	int: The request code passed in requestPermissions(android.app.Activity, String[], int)
     * @param permissions	String: The requested permissions. Never null.
     * @param grantResults	int: The grant results for the corresponding permissions which is either PERMISSION_GRANTED or PERMISSION_DENIED. Never null.
     */
    fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String>,
                                            @NonNull grantResults: IntArray) {
        if (requestCode == 100) {
            RuntimePermissionUtil.onRequestPermissionsResult(grantResults, object : RPResultListener {
                override fun onPermissionDenied() {
                    // do nothing
                }

                override fun onPermissionGranted() {
                    if (RuntimePermissionUtil.checkPermissionGranted(activity, recordAudioPermission)) {
                        hasRecordAudioPermission = true
                    }
                }
            })
        }
    }

    /**
     * Activates all gesture that are enabled in the preference activity
     */
    fun activate() {
        Log.d(LOG_TAG, "Activating gestures")
        Gestures.values().forEach { gesture ->
            if(gesture.isEnabled(activity)) {
                Log.d(LOG_TAG, """Activating gesture:${gesture.name}""")
                gesture.activate(listener, this, activity)
            }
        }
    }

    /**
     * Deactivates all gestures
     */
    fun deactivate() {
        Log.d(LOG_TAG, "Deactivating gestures")
        Gestures.values().forEach { gesture -> gesture.deactivate(listener) }
    }

    /**
     * Destroys all gesture related objects
     */
    fun destroy() {
        Sensey.getInstance().stop()
    }
}