package com.smarties.data.gestures

import android.content.Context
import androidx.preference.PreferenceManager
import com.github.nisrulz.sensey.Sensey
import com.smarties.data.gestures.TweakableValues.LIGHT_DETECTION_THRESHOLD
import com.smarties.data.gestures.TweakableValues.SHAKE_DETECTION_THRESHOLD
import com.smarties.data.gestures.TweakableValues.SHAKE_DETECTION_TIMEOUT

/**
 * Contains members for all gestures and methods to activate and deactivate them
 */
enum class Gestures(private val preferenceName: String) {
    SHAKE("shake_gesture") {
        override fun activate(listener: GestureListener, manager: GestureManager, context: Context) = sensorManager().startShakeDetection(SHAKE_DETECTION_THRESHOLD, SHAKE_DETECTION_TIMEOUT, listener)

        override fun deactivate(listener: GestureListener) {
            sensorManager().stopShakeDetection(listener)
        }
    },
    FLIP("flip_gesture") {
        override fun activate(listener: GestureListener, manager: GestureManager, context: Context) = sensorManager().startFlipDetection(listener)

        override fun deactivate(listener: GestureListener)  = sensorManager().stopFlipDetection(listener)

    },
    ORIENTATION("orientation_gesture") {
        override fun activate(listener: GestureListener, manager: GestureManager, context: Context) = sensorManager().startOrientationDetection(listener)

        override fun deactivate(listener: GestureListener)  = sensorManager().stopOrientationDetection(listener)
    },
    PROXIMITY("proximity_gesture") {
        override fun activate(listener: GestureListener, manager: GestureManager, context: Context) = sensorManager().startProximityDetection(listener)

        override fun deactivate(listener: GestureListener)  = sensorManager().stopProximityDetection(listener)
    },
    LIGHT("light_gesture") {
        override fun activate(listener: GestureListener, manager: GestureManager, context: Context) = sensorManager().startLightDetection(LIGHT_DETECTION_THRESHOLD, listener)

        override fun deactivate(listener: GestureListener)  = sensorManager().stopLightDetection(listener)
    },
    WAVE("wave_gesture") {
        override fun activate(listener: GestureListener, manager: GestureManager, context: Context) = sensorManager().startWaveDetection(listener)

        override fun deactivate(listener: GestureListener)  = sensorManager().stopWaveDetection(listener)
    },
    TOUCH("touch_detection") {
        override fun activate(listener: GestureListener, manager: GestureManager, context: Context) = sensorManager().startTouchTypeDetection(context, listener)

        override fun deactivate(listener: GestureListener) = sensorManager().stopTouchTypeDetection()
    },
    PINCH_SCALE("pinch_scale_gesture") {
        override fun activate(listener: GestureListener, manager: GestureManager, context: Context) = sensorManager().startPinchScaleDetection(context, listener)

        override fun deactivate(listener: GestureListener) = sensorManager().stopPinchScaleDetection()

    },
    SOUND_LEVEL("sound_level_detection") {
        override fun activate(listener: GestureListener, manager: GestureManager, context: Context) {
            if(manager.hasRecordAudioPermission) {
                sensorManager().startSoundLevelDetection(context, listener)
            } else {
                manager.requestAudioPermission()
            }
        }

        override fun deactivate(listener: GestureListener) {
            sensorManager().stopSoundLevelDetection()
        }
    },
    MOVEMENT("movement_detection") {
        override fun activate(listener: GestureListener, manager: GestureManager, context: Context) = sensorManager().startMovementDetection(listener)

        override fun deactivate(listener: GestureListener)  = sensorManager().stopMovementDetection(listener)
    },
    CHOP("chop_detector") {
        override fun activate(listener: GestureListener, manager: GestureManager, context: Context) = sensorManager().startChopDetection(30f, 500, listener)

        override fun deactivate(listener: GestureListener) = sensorManager().stopChopDetection(listener)

    },
    WRIST_TWIST("wrist_twist_detection") {
        override fun activate(listener: GestureListener, manager: GestureManager, context: Context) = sensorManager().startWristTwistDetection(listener)

        override fun deactivate(listener: GestureListener)  = sensorManager().stopWristTwistDetection(listener)
    },
    TILT("tilt_direction_detection") {
        override fun activate(listener: GestureListener, manager: GestureManager, context: Context) = sensorManager().startTiltDirectionDetection(listener)

        override fun deactivate(listener: GestureListener)  = sensorManager().stopTiltDirectionDetection(listener)
    },
    ROTATION_ANGLE("rotation_angle_detection") {
        override fun activate(listener: GestureListener, manager: GestureManager, context: Context) = sensorManager().startRotationAngleDetection(listener)

        override fun deactivate(listener: GestureListener)  = sensorManager().stopRotationAngleDetection(listener)
    };

    /**
     * Enables the sensor
     * @param listener class that handles all gesture events
     * @param context Context
     */
    abstract fun activate(listener: GestureListener, manager: GestureManager, context: Context)

    /**
     * Disables the sensor
     * @param listener class that handles all gesture events
     */
    abstract fun deactivate(listener: GestureListener)

    /**
     * Checks if the gesture is enabled in the preferences
     * @param context Context
     */
    fun isEnabled(context: Context):Boolean {
        val prefManager = PreferenceManager.getDefaultSharedPreferences(context);
        return prefManager.getBoolean(this.preferenceName,false)
    }

    /**
     * Return the sensor library instance
     */
    fun sensorManager() = Sensey.getInstance()!!

}
