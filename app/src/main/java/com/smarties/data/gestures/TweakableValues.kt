package com.smarties.data.gestures

object TweakableValues {
    const val DEFAULT_INC = 50

    const val TEMPERATURE_INC = DEFAULT_INC
    const val BRIGTHNESS_INC = DEFAULT_INC
    const val SATURATION_INC = DEFAULT_INC

    const val VOLUME_TRESHOLD = 60 // db

    const val ROTATION_EVENT_SKIPS = 10

    const val SHAKE_DETECTION_THRESHOLD = 10f
    const val SHAKE_DETECTION_TIMEOUT = 2000L
    const val LIGHT_DETECTION_THRESHOLD = 10f
}