package com.smarties.data.gestures.state

data class HueState(var on: Boolean,
                    var hue: Int,
                    var saturation: Int,
                    var brightness: Int,
                    var colorTemperature: Int)

enum class ColorBounds(val low: Int, val high: Int) {
    HUE(0,65535),
    BRI(1,254),
    SAT(0,254),
    XY(0,1),
    CT(153,500);
}