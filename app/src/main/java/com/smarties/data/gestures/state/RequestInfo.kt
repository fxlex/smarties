package com.smarties.data.gestures.state

data class RequestInfo(val address: String, val userName: String, val id: String, val isRoom: Boolean)
