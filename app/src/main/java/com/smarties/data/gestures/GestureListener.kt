package com.smarties.data.gestures

import com.github.nisrulz.sensey.ChopDetector.ChopListener
import com.github.nisrulz.sensey.FlipDetector.FlipListener
import com.github.nisrulz.sensey.LightDetector.LightListener
import com.github.nisrulz.sensey.MovementDetector.MovementListener
import com.github.nisrulz.sensey.OrientationDetector.OrientationListener
import com.github.nisrulz.sensey.PinchScaleDetector
import com.github.nisrulz.sensey.ProximityDetector.ProximityListener
import com.github.nisrulz.sensey.RotationAngleDetector.RotationAngleListener
import com.github.nisrulz.sensey.ShakeDetector.ShakeListener
import com.github.nisrulz.sensey.SoundLevelDetector.SoundLevelListener
import com.github.nisrulz.sensey.TiltDirectionDetector.TiltDirectionListener
import com.github.nisrulz.sensey.TouchTypeDetector
import com.github.nisrulz.sensey.WaveDetector.WaveListener
import com.github.nisrulz.sensey.WristTwistDetector.WristTwistListener

/**
 * Interface that implements all gesture listeners
 */
interface GestureListener: ShakeListener, FlipListener, LightListener,
OrientationListener, ProximityListener, WaveListener, SoundLevelListener, MovementListener,
ChopListener, WristTwistListener, RotationAngleListener, TiltDirectionListener, PinchScaleDetector.PinchScaleListener,  TouchTypeDetector.TouchTypListener