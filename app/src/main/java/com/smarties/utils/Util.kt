package com.smarties.utils

import android.view.View


object Util {
    /**
     * Determines if given points are inside view
     * @param x - x coordinate of point
     * @param y - y coordinate of point
     * @param view - view object to compare
     * @return true if the points are within view bounds, false otherwise
     */
    fun isPointInsideView(x: Float, y: Float, view: View): Boolean {
        val location = IntArray(2)
        view.getLocationOnScreen(location)
        val viewX = location[0]
        val viewY = location[1]

        //point is inside view bounds
        return x > viewX && x < viewX + view.width && y > viewY && y < viewY + view.height
    }

    /**
     * Re-maps a number from one range to another
     * @param value the incoming value to be converted
     * @param start1 lower bound of the value's current range
     * @param stop1 upper bound of the value's current range
     * @param start2 lower bound of the value's target range
     * @param stop2 upper bound of the value's target range
    */
    fun map(value: Float, start1: Float, stop1: Float, start2: Float, stop2: Float): Float {
        return start2 + (stop2 - start2) * ((value - start1) / (stop1 - start1))
    }

    /**
     *
     * Constrains a value to not exceed a maximum and minimum value.
     *
     * ( end auto-generated )
     * @webref math:calculation
     * @param amt the value to constrain
     * @param low minimum limit
     * @param high maximum limit
     */
    fun constrain(amt: Int, low: Int, high: Int): Int {
        return if (amt < low) low else if (amt > high) high else amt
    }
}