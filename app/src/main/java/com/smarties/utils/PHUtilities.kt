package com.smarties.utils

import android.graphics.Color
import android.graphics.PointF

import java.util.ArrayList

/**
 * Decompiled from Philips Hue SDK resources, to be able to run without the jar dependency.
 */
object PHUtilities {
    private val colorPointsLivingColor = ArrayList<PointF>()
    private val colorPointsHueBulb = ArrayList<PointF>()
    private val colorPointsDefault = ArrayList<PointF>()
    private val HUE_BULBS = ArrayList<String>()
    private val LIVING_COLORS = ArrayList<String>()

    init {
        HUE_BULBS.add("LCT001")
        HUE_BULBS.add("LCT002")
        HUE_BULBS.add("LCT003")
        HUE_BULBS.add("LLM001")

        LIVING_COLORS.add("LLC001")
        LIVING_COLORS.add("LLC005")
        LIVING_COLORS.add("LLC006")
        LIVING_COLORS.add("LLC007")
        LIVING_COLORS.add("LLC010")
        LIVING_COLORS.add("LLC011")
        LIVING_COLORS.add("LLC012")
        LIVING_COLORS.add("LLC014")
        LIVING_COLORS.add("LLC013")
        LIVING_COLORS.add("LST001")

        colorPointsHueBulb.add(PointF(0.674f, 0.322f))
        colorPointsHueBulb.add(PointF(0.408f, 0.517f))
        colorPointsHueBulb.add(PointF(0.168f, 0.041f))

        colorPointsLivingColor.add(PointF(0.703f, 0.296f))
        colorPointsLivingColor.add(PointF(0.214f, 0.709f))
        colorPointsLivingColor.add(PointF(0.139f, 0.081f))

        colorPointsDefault.add(PointF(1.0f, 0.0f))
        colorPointsDefault.add(PointF(0.0f, 1.0f))
        colorPointsDefault.add(PointF(0.0f, 0.0f))
    }

    fun calculateXY(color: Int, model: String): FloatArray {
        var red = 1.0f
        var green = 1.0f
        var blue = 1.0f

        red = Color.red(color) / 255.0f
        green = Color.green(color) / 255.0f
        blue = Color.blue(color) / 255.0f

        val r = if (red > 0.04045f)
            Math.pow(((red + 0.055f) / 1.055f).toDouble(), 2.400000095367432).toFloat()
        else
            red / 12.92f
        val g = if (green > 0.04045f)
            Math.pow(((green + 0.055f) / 1.055f).toDouble(), 2.400000095367432).toFloat()
        else
            green / 12.92f
        val b = if (blue > 0.04045f)
            Math.pow(((blue + 0.055f) / 1.055f).toDouble(), 2.400000095367432).toFloat()
        else
            blue / 12.92f

        val x = r * 0.649926f + g * 0.103455f + b * 0.197109f
        val y = r * 0.234327f + g * 0.743075f + b * 0.022598f
        val z = r * 0.0f + g * 0.053077f + b * 1.035763f

        val xy = FloatArray(2)

        xy[0] = x / (x + y + z)
        xy[1] = y / (x + y + z)
        if (java.lang.Float.isNaN(xy[0])) {
            xy[0] = 0.0f
        }
        if (java.lang.Float.isNaN(xy[1])) {
            xy[1] = 0.0f
        }

        val xyPoint = PointF(xy[0], xy[1])
        val colorPoints = colorPointsForModel(model)
        val inReachOfLamps = checkPointInLampsReach(xyPoint, colorPoints)
        if (!inReachOfLamps) {
            val pAB = getClosestPointToPoints(colorPoints[0],
                    colorPoints[1], xyPoint)
            val pAC = getClosestPointToPoints(colorPoints[2],
                    colorPoints[0], xyPoint)
            val pBC = getClosestPointToPoints(colorPoints[1],
                    colorPoints[2], xyPoint)

            val dAB = getDistanceBetweenTwoPoints(xyPoint, pAB!!)
            val dAC = getDistanceBetweenTwoPoints(xyPoint, pAC!!)
            val dBC = getDistanceBetweenTwoPoints(xyPoint, pBC!!)

            var lowest = dAB
            var closestPoint: PointF = pAB
            if (dAC < lowest) {
                lowest = dAC
                closestPoint = pAC
            }
            if (dBC < lowest) {
                lowest = dBC
                closestPoint = pBC
            }

            xy[0] = closestPoint.x
            xy[1] = closestPoint.y
        }

        xy[0] = precision(xy[0])
        xy[1] = precision(xy[1])
        return xy
    }

    fun precision(d: Float): Float {
        return Math.round(10000.0f * d) / 10000.0f
    }

    fun calculateXYFromRGB(red: Int, green: Int, blue: Int, model: String): FloatArray {
        val rgb = Color.rgb(red, green, blue)
        return calculateXY(rgb, model)
    }

    private fun checkPointInLampsReach(point: PointF?, colorPoints: List<PointF>?): Boolean {
        if (point == null || colorPoints == null) {
            return false
        }
        val red = colorPoints[0]
        val green = colorPoints[1]
        val blue = colorPoints[2]
        val v1 = PointF(green.x - red.x, green.y - red.y)
        val v2 = PointF(blue.x - red.x, blue.y - red.y)
        val q = PointF(point.x - red.x, point.y - red.y)
        val s = crossProduct(q, v2) / crossProduct(v1, v2)
        val t = crossProduct(v1, q) / crossProduct(v1, v2)

        return s >= 0.0f && t >= 0.0f && s + t <= 1.0f
    }

    private fun getDistanceBetweenTwoPoints(one: PointF, two: PointF): Float {
        val dx = one.x - two.x
        val dy = one.y - two.y

        return Math.sqrt((dx * dx + dy * dy).toDouble()).toFloat()
    }

    private fun crossProduct(point1: PointF, point2: PointF): Float {
        return point1.x * point2.y - point1.y * point2.x
    }

    private fun colorPointsForModel(model: String?): List<PointF> {
        var model = model
        if (model == null) {
            model = " "
        }
        val colorPoints: List<PointF>
        if (HUE_BULBS.contains(model)) {
            colorPoints = colorPointsHueBulb
        } else {
            if (LIVING_COLORS.contains(model)) {
                colorPoints = colorPointsLivingColor
            } else {
                colorPoints = colorPointsDefault
            }
        }
        return colorPoints
    }

    private fun getClosestPointToPoints(pointA: PointF?, pointB: PointF?, pointP: PointF?): PointF? {
        if (pointA == null || pointB == null || pointP == null) {
            return null
        }
        val pointAP = PointF(pointP.x - pointA.x, pointP.y - pointA.y)
        val pointAB = PointF(pointB.x - pointA.x, pointB.y - pointA.y)
        val ab2 = pointAB.x * pointAB.x + pointAB.y * pointAB.y
        val apAb = pointAP.x * pointAB.x + pointAP.y * pointAB.y
        var t = apAb / ab2
        if (t < 0.0f) {
            t = 0.0f
        } else if (t > 1.0f) {
            t = 1.0f
        }
        return PointF(pointA.x + pointAB.x * t, pointA.y + pointAB.y * t)
    }


}