# Home App for Android™
HomeApp is a small and easy to use smart home app with a simple framework. The goal of this application is to make remote execution of predefined features as easy and user-friendly as possible to help you get started with smart home technology.  
Communication between the devices uses HTTP requests and JSON strings. After the commanding device has send a HTTP request to the smart home device, the smart home device sends back a JSON string containing the information the app needs.  
This app is especially useful if you are using microcontrollers or other small devices such as the Raspberry Pi for smart home automation.  
 
Compatible with Philips Hue!  
<br />
<img src="https://domi04151309.github.io/images/Android/HomeApp1.jpg" width="18%" />
<img src="https://domi04151309.github.io/images/Android/HomeApp2.jpg" width="18%" />
<img src="https://domi04151309.github.io/images/Android/HomeApp3.jpg" width="18%" />
<img src="https://domi04151309.github.io/images/Android/HomeApp4.jpg" width="18%" />
<img src="https://domi04151309.github.io/images/Android/HomeApp5.jpg" width="18%" />
<img src="https://domi04151309.github.io/images/Android/HomeApp6.jpg" width="18%" />  
 
<i style="color:gray;">Android is a trademark of Google LLC.</i>
